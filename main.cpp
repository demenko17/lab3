#include <iostream>
#include "windows.h"
using namespace std;

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int num, c1, c2, c3;
    cout << "������ �����: ";
    cin >> num;
    c1 = num / 100;
    c2 = num % 100 / 10;
    c3 = num % 10;
    cout << "�� ����� ����� " << num << " ? " << (c1 != c2 && c2 != c3) << endl;
    cout << "�� ����� ����� " << num << " ? " << boolalpha << (c1 != c2 == c2 != c3) << endl;
    cout << "�� ����� ����� ";
    (c1 != c2 && c2 != c3) ? cout << " ����\n" : cout << " �������\n";
    return 0;
}
